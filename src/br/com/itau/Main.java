package br.com.itau;

import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random rd = new Random();
        System.out.println("Etapa 01 - " + rd.nextInt(6));

        String titulo = "Etapa 02 - ";

        for (int n = 0; n <=3; n++){
            ArrayList<Integer> listaNumeros = new ArrayList();
            int soma = 0;
            for (int i = 0; i <=2; i++){
                int num = rd.nextInt(6) +1;
                listaNumeros.add(num);
                soma += num;
            }
            if(n > 0){
                titulo = "Etapa 03 - ";
            }
            System.out.println(titulo + listaNumeros.get(0) + ", " + listaNumeros.get(1) + ", " + listaNumeros.get(2) + ", " +
                    soma);
        }

    }
}
